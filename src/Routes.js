import React, { Component } from "react"
import { connect } from "react-redux"
import { bindActionCreators } from "redux"
import { Route, Switch, withRouter } from "react-router-dom"

import "./styles/App.css"
import * as actions from "./actions/AuthActions"
import RequireAuth from "./components/RequireAuth"
import SignIn from "./pages/SignIn"
import Catalogues from "./pages/Catalogues"
import Catalogue from "./pages/Catalogue"

class Routes extends Component {
  componentWillMount() {
    this.props.actions.signInAnnonim()
  }
  render() {
    return (
      <Switch>
        <Route exact path="/" component={RequireAuth(Catalogues)} />
        <Route path="/:productID" component={RequireAuth(Catalogue)} />
        <Route path="/signin" component={SignIn} />
      </Switch>
    )
  }
}
function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch)
  }
}
function mapStateToProps(state) {
  return {
    isLogged: !state.auth.user,
    ...state.auth
  }
}
export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Routes)
)
