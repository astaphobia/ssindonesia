import React from "react"
import ReactDOM from "react-dom"
import { Provider } from "react-redux"
import { BrowserRouter } from "react-router-dom"
import 'bootstrap/dist/css/bootstrap.css'
import 'material-design-icons/iconfont/material-icons.css'

import Routes from "./Routes"
import registerServiceWorker from "./registerServiceWorker"
import "./styles/index.css"
import store from "./store"
import TopNavigation from "./components/TopNavigation";

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <React.Fragment>
        <TopNavigation />
        <main className="main">
          <div className="centered">
            <Routes />
          </div>
        </main>
      </React.Fragment>
    </BrowserRouter>
  </Provider>,
  document.getElementById("root")
)
registerServiceWorker()
